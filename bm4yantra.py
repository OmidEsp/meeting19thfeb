from __future__ import division,print_function
__doc__= """ 
Benchmark: single component reactive transport model with moving boundary  (dissolution)
"""
#%% import modules
import sys,os
PARENT1 = '../'
YANTRA_PATH = '/home/ravi/ownCloud/codes/yantra_dev/yantra-dev'
sys.path.append(os.path.join(os.path.dirname(__file__), PARENT1))
sys.path.append(YANTRA_PATH)
import yantra
from SingleCompReactTrans import SingleCompReactTrans
from scipy.special import erfc
from scipy.optimize import fsolve
import numpy as np
import matplotlib.pylab as plt
#%%Analytical solution
#reference:
#Aaron, et al. (1970). Diffusion-Limited Phase Transformations: 
#A Comparison and Critical Evaluation of the Mathematical Approximations.
#Journal of applied physics, 41(11), 4404-4410.

#%% parameters for the simulations
ci= 0.5
cm= 0
cp= 1
dx= 1
x0= 10
D = 1./6.
kr=1e-5
#%%create yantra domain instance
domain = yantra.Domain2D((0,0),(200,2),dx, grid_type = 'midway')
x = domain.x
xx, _ = domain.meshgrid()
domain.nodetype =1. * (xx >= x0)
cs = cp* (domain.nodetype > 0)
#%%create physics instance
#domain params 
domain_params={}
domain_params['D'] = D
domain_params['rxntype']='kin'
domain_params['ceq']=ci
domain_params['c']=cm
domain_params['mv']=1/cp
domain_params['thres']=0.5
domain_params['cs']=cs
domain_params['kr']=kr
#set boundary conditions
#concentration 0  on left boundary and zero flux on right hand boundary
bc_params ={}
bc_params['left']=['c' ,0.0]
bc_params['right']=['flux',0.0]
#solver parameters
solver_params={}
solver_params['lattice']='D2Q5'
solver_params['collision_model']='srt'

#create physics instance
rt = SingleCompReactTrans(domain,domain_params,bc_params,solver_params)

#%%run model
dxim=[]
tl = []
xlb0=x[np.argmax(rt.nodetype[1,:])]
while rt.time <=500000:
    rt.advance()
    t = rt.time
    tl.append(t)
    dxim.append(x[np.argmax(rt.nodetype[1,:])]-xlb0-dx/2) #-dx/2 added gives center of location of fluid interface node

#%%plot output    
tf =t= rt.time
cm = rt.c[1,:]+rt.cs[1,:]
plt.figure()
plt.plot(x,cm,'--',label = 'model')
plt.legend()
plt.show()
#dxim=np.asarray(dxim)
#dxim = dxim*dx
plt.figure()
plt.plot(tl,dxim,'--',label = 'model')
plt.legend()
plt.title("delta x: %s       rxntype: %s"%(dx,rt.rxntype))    
plt.show()
