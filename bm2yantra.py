#!/usr/bin/python
# -*- coding: utf-8 -*-
#=======================================================================================
#This File is part of Yantra: A lattice Boltzmann method based tool for multiscale/
#multiphyics simulations
#=======================================================================================
#
#Copyright (C) 2016-2017  <Author> Ravi A. Patel <Email> ravee.a.patel@gmail.com
#
#This program is free software: you can redistribute it and/or modify it under the
#terms of the GNU General Public License as published by the Free Software 
#Foundation, either version 3 of the License, or any later version.
#This program is distributed in the hope that it will be useful, but WITHOUT ANY 
#WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
#PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#
#=======================================================================================

from __future__ import division,print_function
__doc__= """ 
Benchmark 1: Comparision of 2D implementation of Yantra's Advection diffusion equation \
 with 1D analytical solution for infinite domain with constant concentration  \
 boundary conditions 
"""
#%% import modules 
import sys,os
PARENT = '..'
sys.path.append(os.path.join(os.path.dirname(__file__), PARENT))
import yantra
import numpy as np
from numpy import exp
from scipy.special import erfc 
import matplotlib.pylab as plt

#%%create yantra domain instance
dx=1
domain = yantra.Domain2D((0,0),(1000,100),dx, grid_type = 'nodal')


#%% Define physics
cb = 0
D=1./6.
u=[0.029, 0]
#domain params 
domain_params={}
domain_params['D'] = D
domain_params['u'] = u

#set boundary conditions
#concentration gradient along x-axis all other boundary periodic
bc_params ={}
bc_params['left']=['c' ,1]
bc_params['top']=['flux' ,0]
bc_params['bottom']=['c' ,0]
#solver parameters
solver_params={}
solver_params['lattice']='D2Q5'
solver_params['collision_model']='srt' #other options 'trt' and 'diff_vel'
#create physics instance
ade = yantra.AdvectionDiffusion(domain,domain_params,bc_params,solver_params)
#%%run model

niters=3000
for i in range(niters):
    ade.run(time=i)
    
cm =ade.c
flux=ade.flux[0,:,0]
#%%plot results
plt.plot(range(101),flux)
#plt.colorbar()  
plt.show()
plt.imshow(cm)
plt.colorbar()
plt.show()