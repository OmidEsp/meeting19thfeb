#!/usr/bin/python
# -*- coding: utf-8 -*-
#=======================================================================================
#This File is part of Yantra: A lattice Boltzmann method based tool for multiscale/
#multiphyics simulations
#=======================================================================================
#
#Copyright (C) 2016-2017  <Author> Ravi A. Patel <Email> ravee.a.patel@gmail.com
#
#This program is free software: you can redistribute it and/or modify it under the
#terms of the GNU General Public License as published by the Free Software 
#Foundation, either version 3 of the License, or any later version.
#This program is distributed in the hope that it will be useful, but WITHOUT ANY 
#WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
#PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#
#=======================================================================================

from __future__ import division,print_function
__doc__= """ 
Benchmark 1: Comparision of 2D implementation of Yantra's Advection diffusion equation \
 with 1D analytical solution for infinite domain with constant concentration  \
 boundary conditions 
"""
#%% import modules 
import sys,os
PARENT = '..'
sys.path.append(os.path.join(os.path.dirname(__file__), PARENT))
import yantra
import numpy as np
from numpy import exp
from scipy.special import gamma 
import matplotlib.pylab as plt
flux=[]
#flux=np.asarray(flux)
#%%create yantra domain instance
for i in np.arange(1000)+1:
    j=int(round(i*100))
    res=(gamma(4./3.)*(9**1./3.))**(-1) * (4*178/((i)*0.001))**(1./3.)
    flux.append(res)

plt.figure()
plt.plot(np.arange(1000)/1000,flux, label = 'analytical')
#plt.plot(range(niters),dxim,'--', label = 'Yantra')
plt.show()